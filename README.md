**Case Sul America**

**Informações sobre o projeto**

1. Linguagens Open JDK 11, utilizando as ferramentas do spring boot 2.5.5 e MongoDB para gerenciamentos dos dados.
2. Para rodar o projeto precisa do lombok instalado.
3. Para realizar as chamadas ao sistema utilize o Postman ou o Swagger que pode ser acessado pela url http://localhost:8080/swagger-ui.html#/ o arquivo para import já consta dentro da branch (Sul America.postman_collection.json).
4. O processamento de gerar os log's do exame está configurado para todos os dias as 08:00.

## Versão 1.0.0
---