package com.br.sulAmerica.api.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public enum EnumErrorException {

	PARAMETROS_INVALIDOS(HttpStatus.CONFLICT,
			"Erro", 
			"Informações inválidas",
			"O corpo e/ou parametros da requisição inválidos"),
	
	ASSEMBLEIA_FINALIZADA(HttpStatus.CONFLICT,
			"Erro", 
			"Informações inválidas",
			"Está assembleia/pauta já está finalizada!"),
	
	VOTO_JA_REALIZADO(HttpStatus.CONFLICT,
			"Erro", 
			"Informações inválidas",
			"Voto já realizado"),
	
	ERROR_VALIDA_CPF_API(HttpStatus.BAD_REQUEST,
			"Erro",
			"Erro ao validar o CPF",
			"Erro ao validar o CPF na API!"),
	
	ERROR_SAVE(HttpStatus.BAD_REQUEST,
			"Erro",
			"Erro ao salvar",
			"Erro ao salvar o registro no banco!"),
	
	ERROR_DELETE(HttpStatus.BAD_REQUEST,
			"Erro",
			"Erro ao excluir",
			"Erro ao excluir o registro no banco!");

	EnumErrorException(HttpStatus httpStatus, String tipo, String nome, String detalhe) {
		this.httpStatus = httpStatus;
		this.tipo = tipo;
		this.nome = nome;
		this.detalhe = detalhe;
	}

	private HttpStatus httpStatus;

	private String nome;

	private String detalhe;

	private String tipo;

}
