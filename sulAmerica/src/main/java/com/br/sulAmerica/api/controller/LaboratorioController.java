package com.br.sulAmerica.api.controller;

import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.sulAmerica.domain.model.Laboratorio;
import com.br.sulAmerica.service.LaboratorioService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/laboratorios")
public class LaboratorioController {

	@Autowired
	private LaboratorioService service;
	
	@ApiOperation(value = "Lista todos os laboratórios")
	@GetMapping
	public ResponseEntity<?> listarTodos() {
		return ResponseEntity.ok(service.listarTodos());
	}
	
	@ApiOperation(value = "Busca um laboratório pelo cnpj")
	@GetMapping(path = "/{cnpj}")
	public ResponseEntity<Laboratorio> buscarPorCnpj(@PathVariable String cnpj) {
		Laboratorio laboratorio = service.buscarPorCnpj(cnpj);
		if(Objects.isNull(laboratorio)) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(laboratorio);
		}
	}
	
	@ApiOperation(value = "Salvar um laboratório")
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(produces="application/json")
	public ResponseEntity<?> salvar(@Valid @RequestBody Laboratorio laboratorio) {
		return ResponseEntity.ok(service.salvar(laboratorio));
	}
	
}
