package com.br.sulAmerica.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.sulAmerica.domain.dto.ExameDTO;
import com.br.sulAmerica.service.ExameLogService;
import com.br.sulAmerica.service.ExameService;

@RestController
@RequestMapping("/exames")
public class ExameController {

	@Autowired
	private ExameService service;
	
	@Autowired
	private ExameLogService exameLogService;
	
	@GetMapping
	public ResponseEntity<?> listarTodos() {
		return ResponseEntity.ok(service.listarTodos());
	}
	
	@PostMapping(produces="application/json")
	public ResponseEntity<?> salvar(@Valid @RequestBody ExameDTO dto) {
		return ResponseEntity.ok(service.salvar(dto));
	}
	
	@GetMapping("/listarExameLog")
	public ResponseEntity<?> recuperarExameLog() {
		return ResponseEntity.ok(exameLogService.listaTodosPorDia());
	}
}
