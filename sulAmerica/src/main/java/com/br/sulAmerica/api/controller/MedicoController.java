package com.br.sulAmerica.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.br.sulAmerica.domain.dto.MedicoDTO;
import com.br.sulAmerica.service.MedicoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private MedicoService service;
	
	@ApiOperation(value = "Lista todos os médicos")
	@GetMapping
	public ResponseEntity<?> listarTodos() {
		return ResponseEntity.ok(service.listarTodos());
	}
	
	@ApiOperation(value = "Salva um médico")
	@ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping(produces="application/json")
	public ResponseEntity<?> salvar(@Valid @RequestBody MedicoDTO dto) {
		return ResponseEntity.ok(service.salvar(dto));
	}
	
}
