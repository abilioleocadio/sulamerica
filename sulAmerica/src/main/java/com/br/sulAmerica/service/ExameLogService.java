package com.br.sulAmerica.service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sulAmerica.domain.model.Exame;
import com.br.sulAmerica.domain.model.ExameLog;
import com.br.sulAmerica.domain.repository.ExameLogRepository;

@Service
public class ExameLogService {

	@Autowired
	private ExameLogRepository repository;
	
	public void gerarExameLog(Exame exame) {

		ExameLog exameLog = new ExameLog();
		exameLog.setId(UUID.randomUUID());
		exameLog.setLaboratorio(exame.getMedico().getLaboratorio().getRazaoSocial());
		exameLog.setMedico(exame.getMedico().getNome());
		exameLog.setPaciente(exame.getPaciente().getNome());
		exameLog.setTipoExame(exame.getTipo());
		exameLog.setDataHoraInicial(exame.getDataHoraInicial());
		exameLog.setDataHoraFinal(exame.getDataHoraFinal());
		
		System.out.println("Exame Log: " + exameLog);
		
		repository.save(exameLog);
	}

	public List<ExameLog> listaTodosPorDia() {
		return repository.listarTodosPorDia(LocalDate.now());
	}
	
}
