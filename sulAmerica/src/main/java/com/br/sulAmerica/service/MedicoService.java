package com.br.sulAmerica.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.sulAmerica.api.exception.EnumErrorException;
import com.br.sulAmerica.api.exception.ServiceException;
import com.br.sulAmerica.domain.dto.MedicoDTO;
import com.br.sulAmerica.domain.model.Laboratorio;
import com.br.sulAmerica.domain.model.Medico;
import com.br.sulAmerica.domain.repository.MedicoRepository;

@Service
public class MedicoService {

	@Autowired
	private MedicoRepository repository;
	
	@Autowired
	private LaboratorioService laboratorioService;

	public List<Medico> listarTodos() {
		return repository.findAll();
	}

	@Transactional
	public Medico salvar(MedicoDTO dto) {
		Medico medico = new Medico();
		BeanUtils.copyProperties(dto, medico, "laboratorioCnpj");
		
		Laboratorio laboratorio = laboratorioService.buscarPorCnpj(dto.getLaboratorioCnpj());
		if(Objects.isNull(laboratorio)) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Laboratório não encontrado!");
		}
		medico.setLaboratorio(laboratorio);
		
		return repository.save(medico);
	}

	public Medico buscarPorCrm(String medicoCrm) {
		return repository.findById(medicoCrm).orElse(null);
	}

}
