package com.br.sulAmerica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.br.sulAmerica.domain.model.Laboratorio;
import com.br.sulAmerica.domain.repository.LaboratorioRepository;

@Service
public class LaboratorioService {

	@Autowired
	private LaboratorioRepository repository;

	public List<Laboratorio> listarTodos() {
		return repository.findAll();
	}

	public Laboratorio buscarPorCnpj(String cnpj) {
		return repository.findById(cnpj).orElse(null);
	}

	@Transactional
	public Laboratorio salvar(Laboratorio laboratorio) {
		return repository.save(laboratorio);
	}
	
}
