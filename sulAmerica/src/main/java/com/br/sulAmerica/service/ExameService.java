package com.br.sulAmerica.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sulAmerica.api.exception.EnumErrorException;
import com.br.sulAmerica.api.exception.ServiceException;
import com.br.sulAmerica.domain.dto.ExameDTO;
import com.br.sulAmerica.domain.enums.EnumTipoTempoConsulta;
import com.br.sulAmerica.domain.model.Exame;
import com.br.sulAmerica.domain.model.Medico;
import com.br.sulAmerica.domain.model.Paciente;
import com.br.sulAmerica.domain.repository.ExameRepository;

@Service
public class ExameService {

	@Autowired
	private ExameRepository repository;
	
	@Autowired
	private MedicoService medicoService;
	
	@Autowired
	private PacienteService pacienteService;

	public List<Exame> listarTodos() {
		return repository.findAll();
	}

	public Exame salvar(ExameDTO dto) {
		return repository.save(preencherExame(dto));
	}

	private Exame preencherExame(ExameDTO dto) {
		Exame exame = new Exame();
		BeanUtils.copyProperties(dto, exame, "medicoCrm", "pacienteCpf");

		Medico medico = medicoService.buscarPorCrm(dto.getMedicoCrm());
		if(Objects.isNull(medico)) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Medico não encontrado!");
		}
		exame.setMedico(medico);
		
		Paciente paciente = pacienteService.buscarPorCpf(dto.getPacienteCpf());
		if(Objects.isNull(paciente)) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Paciente não encontrado!");
		}
		exame.setPaciente(paciente);
		
		if(exame.getTempoConsulta() != null) {
			adicionandoPeriodoConsulta(exame);
		} else {
			exame.setDataHoraFinal(LocalDateTime.now().plusMinutes(30));
		}
		exame.setId(UUID.randomUUID());
		
		return exame;
	}

	private void adicionandoPeriodoConsulta(Exame exame) {
		
		Exame exameAgendado = buscarPorDataHoraInicial(exame.getDataHoraInicial());
		if(!Objects.isNull(exameAgendado)) {
			throw new ServiceException(EnumErrorException.ERROR_SAVE, "Data hora do exame já agendado!");
		}
		
		if(EnumTipoTempoConsulta.HORA.equals(exame.getTipoTempoConsulta())) {
			exame.setDataHoraFinal(exame.getDataHoraInicial().plusHours(exame.getTempoConsulta()));
		} else if(EnumTipoTempoConsulta.MINUTO.equals(exame.getTipoTempoConsulta())) {
			exame.setDataHoraFinal(exame.getDataHoraInicial().plusMinutes(exame.getTempoConsulta()));
		}
	}

	private Exame buscarPorDataHoraInicial(LocalDateTime dataHoraInicial) {
		return repository.findByDataHoraInicial(dataHoraInicial);
	}

	public List<Exame> listaTodosPorDia() {
		return repository.findAllByDataHoraInicial(LocalDate.now());
	}

}
