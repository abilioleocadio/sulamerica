package com.br.sulAmerica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.sulAmerica.domain.model.Paciente;
import com.br.sulAmerica.domain.repository.PacienteRepository;

@Service
public class PacienteService {

	@Autowired
	private PacienteRepository repository;

	public List<Paciente> listarTodos() {
		return repository.findAll();
	}

	public Paciente salvar(Paciente paciente) {
		return repository.save(paciente);
	}

	public Paciente buscarPorCpf(String pacienteCpf) {
		return repository.findById(pacienteCpf).orElse(null);
	}
	
}
