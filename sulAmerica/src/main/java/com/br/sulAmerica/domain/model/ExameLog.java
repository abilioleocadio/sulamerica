package com.br.sulAmerica.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Document(collection = "exameLog")
public class ExameLog {
	
	@Id
	private UUID id;

	@NotBlank(message = "Nome do laboratório é obrigatório!")
	private String laboratorio;
	
	@NotBlank(message = "Nome do médico é obrigatório!")
	private String medico;
	
	@NotBlank(message = "Nome do paciente é obrigatório!")
	private String paciente;
	
	@NotBlank(message = "Tipo do exame é obrigatório!")
	private String tipoExame;

	@NotNull(message = "Data hora Inicial é obrigatório!")
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime dataHoraInicial;
	
	@NotNull(message = "Data hora final é obrigatório!")
	@JsonFormat(pattern="dd/MM/yyyy HH:mm")
	private LocalDateTime dataHoraFinal;
	
}
