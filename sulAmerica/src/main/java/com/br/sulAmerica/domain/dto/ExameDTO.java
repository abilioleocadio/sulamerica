package com.br.sulAmerica.domain.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.br.sulAmerica.domain.enums.EnumTipoTempoConsulta;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ExameDTO {

	@ApiModelProperty(value = "CRM do médico")
	@NotBlank(message = "CRM do médico é obrigatório!")
	private String medicoCrm;
	
	@ApiModelProperty(value = "CPF do paciente")
	@NotBlank(message = "CPF do paciente é obrigatório!")
	private String pacienteCpf;
	
	@ApiModelProperty(value = "Tipo do exame que será realizado!")
	@NotBlank(message = "Tipo do Exame é obrigatório!")
	private String tipo;
	
	@ApiModelProperty(value = "Data inicial do exame!")
	@JsonFormat(pattern="dd/MM/yyyy HH:mm") // yyyy-MM-dd HH:mm:ss
	@NotNull(message = "Data hora inicial é obrigatória!")
	private LocalDateTime dataHoraInicial;
	
	@ApiModelProperty(value = "Tipo do tempo da consulta 0=HORA | 1=MINUTOS !")
	@NotNull(message = "Periodo do tempo de consulta é obrigatório!")
	private EnumTipoTempoConsulta tipoTempoConsulta;
	
	@ApiModelProperty(value = "Tempo total do exame!")
	@NotNull(message = "Tempo da Consulta é obrigatório!")
	private Integer tempoConsulta;
	
}

