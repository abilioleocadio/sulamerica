package com.br.sulAmerica.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.br.sulAmerica.domain.model.Laboratorio;

public interface LaboratorioRepository extends MongoRepository<Laboratorio, String> {

}
