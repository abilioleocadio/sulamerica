package com.br.sulAmerica.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.br.sulAmerica.domain.enums.EnumTipoTempoConsulta;

import lombok.Data;

@Data
@Document(collection = "exame")
public class Exame {

	@Id
	private UUID id;
	
	@NotNull(message = "Médico é obrigatório!")
	private Medico medico;
	
	@NotNull(message = "Paciente é obrigatório!")
	private Paciente paciente;
	
	@NotBlank(message = "Tipo é obrigatório!")
	private String tipo;
	
	@NotNull(message = "Data hora inicial é obrigatória!")
	private LocalDateTime dataHoraInicial;
	
	@NotNull(message = "Data hora final é obrigatória!")
	private LocalDateTime dataHoraFinal;
	
	@NotNull(message = "Tipo do tempo da consulta é obrigatória!")
	private EnumTipoTempoConsulta tipoTempoConsulta;
	
	@NotNull(message = "Tempo da Consulta é obrigatória!")
	private Integer tempoConsulta;
	
}
