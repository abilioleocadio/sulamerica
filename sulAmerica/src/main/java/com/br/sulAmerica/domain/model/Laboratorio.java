package com.br.sulAmerica.domain.model;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CNPJ;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Document(collection = "laboratorio")
public class Laboratorio {

	@ApiModelProperty(value = "CNPJ do laboratório!")
	@Id
	@CNPJ(message = "CNPJ é inválido!")
	@NotBlank(message = "CNPJ é obrigatório!")
	private String cnpj; // cnpj
	
	@ApiModelProperty(value = "Razão Social do laboratório!")
	@NotBlank(message = "Razão Social é obrigatória!")
	private String razaoSocial;
}
