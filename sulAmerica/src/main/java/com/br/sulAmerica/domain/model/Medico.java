package com.br.sulAmerica.domain.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "medico")
public class Medico {

	@Id
	@NotBlank(message = "CRM é obrigatório!")
	private String crm;
	
	@NotBlank(message = "Nome é obrigatório!")
	private String nome;
	
	@NotNull(message = "Laboratório é obrigatório!")
	private Laboratorio laboratorio;
	
}
