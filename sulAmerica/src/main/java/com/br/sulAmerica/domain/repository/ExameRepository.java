package com.br.sulAmerica.domain.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.br.sulAmerica.domain.model.Exame;

public interface ExameRepository extends MongoRepository<Exame, UUID> {

	public Exame findByDataHoraInicial(LocalDateTime dataHoraInicial);

	@Query("{'dataHoraInicial': {$gte: ?0}}")
	public List<Exame> findAllByDataHoraInicial(LocalDate data);

}
