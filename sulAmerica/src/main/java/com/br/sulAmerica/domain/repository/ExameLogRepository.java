package com.br.sulAmerica.domain.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.br.sulAmerica.domain.model.ExameLog;

public interface ExameLogRepository extends MongoRepository<ExameLog, UUID> {

	@Query("{'dataHoraInicial': {$gte: ?0}}")
	public List<ExameLog> listarTodosPorDia(LocalDate data);

}
