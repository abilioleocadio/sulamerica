package com.br.sulAmerica.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.br.sulAmerica.domain.model.Medico;

public interface MedicoRepository extends MongoRepository<Medico, String> {

}
