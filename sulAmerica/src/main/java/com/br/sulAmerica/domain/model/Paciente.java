package com.br.sulAmerica.domain.model;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@Document(collection = "paciente")
public class Paciente {

	@ApiModelProperty(value = "CPF do paciente!")
	@Id
	@CPF(message = "CPF é inválido!")
	@NotBlank(message = "CPF é obrigatório!")
	private String cpf;
	
	@ApiModelProperty(value = "Nome do paciente!")
	@NotBlank(message = "Nome é obrigatório!")
	private String nome;
	
}
