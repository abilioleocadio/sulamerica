package com.br.sulAmerica.domain.enums;

public enum EnumTipoTempoConsulta {

	HORA(0),
	MINUTO(1);
	
	private Integer value;

	public static EnumTipoTempoConsulta get(Integer value) {
		if (value == null) {
			return null;
		}

		for (EnumTipoTempoConsulta tipo : EnumTipoTempoConsulta.values()) {
			if (tipo.getValue().equals(value)) {
				return tipo;
			}
		}
		return null;
	}
	
	EnumTipoTempoConsulta(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
	
}
