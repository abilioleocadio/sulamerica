package com.br.sulAmerica.domain.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MedicoDTO {

	@ApiModelProperty(value = "CRM do médico!")
	@NotBlank(message = "CRM é obrigatório!")
	private String crm;
	
	@ApiModelProperty(value = "Nome do médico!")
	@NotBlank(message = "Nome é obrigatório!")
	private String nome;
	
	@ApiModelProperty(value = "CNPJ do laboratório!")
	@NotBlank(message = "CNPJ do laboratório é obrigatório!")
	private String laboratorioCnpj;
	
}
