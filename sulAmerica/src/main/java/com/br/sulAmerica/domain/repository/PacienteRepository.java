package com.br.sulAmerica.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.br.sulAmerica.domain.model.Paciente;

public interface PacienteRepository extends MongoRepository<Paciente, String> {

}
