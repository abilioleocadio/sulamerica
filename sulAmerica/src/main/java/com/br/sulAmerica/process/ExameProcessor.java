package com.br.sulAmerica.process;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.br.sulAmerica.domain.model.Exame;
import com.br.sulAmerica.service.ExameLogService;
import com.br.sulAmerica.service.ExameService;

@Component
@EnableScheduling
public class ExameProcessor {

	@Autowired
	private ExameService exameservice;
	
	@Autowired
	private ExameLogService exameLogService;
	
	@Scheduled(cron = "0 0 8 * * *") //  0 */1 * * * * -- 1 Minuto | 0 0 8 * * * -- Todo dia as 8
	public void gerarExameLog() {
		List<Exame> exames = exameservice.listaTodosPorDia();
		exames.forEach(exame -> exameLogService.gerarExameLog(exame));
	}

}
