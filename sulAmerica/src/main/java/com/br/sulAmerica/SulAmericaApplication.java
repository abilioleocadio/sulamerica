package com.br.sulAmerica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SulAmericaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SulAmericaApplication.class, args);
	}

}
